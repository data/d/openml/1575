# OpenML dataset: ijcnn

https://www.openml.org/d/1575

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Danil Prokhorov.  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**: Danil Prokhorov.
IJCNN 2001 neural network competition. 
Slide presentation in IJCNN'01, Ford Research Laboratory, 2001.
http://www.geocities.com/ijcnn/nnc_ijcnn01.pdf .  

#Dataset from the LIBSVM data repository.

Preprocessing:  We use winner's transformation

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1575) of an [OpenML dataset](https://www.openml.org/d/1575). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1575/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1575/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1575/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

